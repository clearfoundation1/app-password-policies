<?php

$lang['password_policies_account_lockout'] = 'Блокування облікового запису';
$lang['password_policies_app_description'] = 'Додаток «Політики паролів» використовується для точного налаштування правил політики паролів користувачів. Це забезпечує застосування паролів, які відповідають вашим критеріям, і допомагає зробити вашу систему більш безпечною.';
$lang['password_policies_app_name'] = 'Політики паролів';
$lang['password_policies_history_size'] = 'Розмір історії';
$lang['password_policies_maximum_password_age'] = 'Максимальний вік пароля';
$lang['password_policies_minimum_password_age'] = 'Мінімальний вік пароля';
$lang['password_policies_minimum_password_length'] = 'Мінімальна довжина пароля';
$lang['password_policies_modify_any_time'] = 'Змінювати в будь-який час';
$lang['password_policies_no_expire'] = 'Без терміну дії';
$lang['password_policies_no_history'] = 'Без історії';
